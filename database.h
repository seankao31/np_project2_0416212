#pragma once
#include <arpa/inet.h>
#include <string>
#include <vector>
#include "serverlib.h"

class Database {
private:
    Database(int nfds): nfds(nfds), ssock(nfds + 1), cli(nfds + 1), valid(nfds + 1, false),
                        pipe(nfds + 1, std::vector<std::string>(nfds + 1)),
                        pipe_occupied(nfds + 1, std::vector<bool>(nfds + 1, false)) {}

public:
    int nfds;
    std::vector<bool> valid;
    std::vector<int> ssock;
    std::vector<Client> cli;

    std::vector<std::vector<bool> > pipe_occupied;
    std::vector<std::vector<std::string> > pipe; // pipe[from][to]

    ~Database() {}
    Database(Database const&) = delete;
    void operator=(Database const&) = delete;

    static Database& GetInstance(int nfds = 1024) {
        static Database instance(nfds);
        return instance;
    }

    // fd, id, addr
    void new_client(int, int, struct sockaddr_in);
    // fd
    void delete_client(int);

    Client& get_client(int);

    void set_name(int, const std::string&);

    int get_id(int);

    // from_id(sender), to_id, output, command line
    // return 0 if success
    int pipe_to(int, int, const std::string&, const std::string&);

    // from_id, to_id(receiver), the string to return, command line
    // return 0 if success
    int pipe_from(int, int, std::string&, const std::string&);
};
