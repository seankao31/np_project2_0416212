#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <vector>
#include "database.h"
#include "serverlib.h"

#define DEMO

void Database::new_client(int fd, int id, struct sockaddr_in addr) {
    cli[fd].id = id;
    cli[fd].name = "(no name)";
#ifdef DEMO
    cli[fd].ip = "CGILAB";
    cli[fd].port = 511;
#else
    char tmp_ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &addr.sin_addr, tmp_ip, INET_ADDRSTRLEN);
    cli[fd].ip = tmp_ip;
    cli[fd].port = addr.sin_port;
#endif
    ssock[id] = fd;
    valid[id] = true;
}

void Database::delete_client(int fd) {
    valid[cli[fd].id] = false;
    for (int id = 1; id <= nfds; ++id) {
        pipe_occupied[cli[fd].id][id] = false;
        pipe_occupied[id][cli[fd].id] = false;
    }
}

Client& Database::get_client(int fd) {
    if (valid[cli[fd].id])
        return cli[fd];
    std::cerr << "Can Not Get Client: Invalid Client" << std::endl;
}

void Database::set_name(int fd, const std::string &str) {
    for (int id = 1; id <= nfds; ++id) {
        if (id != get_id(fd) && valid[id] && str == cli[ssock[id]].name) {
            name_fail_unicast(str, cli[fd].id);
            return;
        }
    }
    cli[fd].name = str;
    name_success_broadcast(cli[fd]);
}

int Database::get_id(int fd) {
    if (valid[cli[fd].id])
        return cli[fd].id;
    return -1;
}

int Database::pipe_to(int sender_id, int to_id, const std::string &output, const std::string &command) {
    if (!valid[to_id]) {
        user_not_exist_unicast(sender_id, to_id);
        return 1;
    }
    else if (pipe_occupied[sender_id][to_id]) {
        pipe_exist_unicast(sender_id, to_id);
        return 1;
    }
    else {
        pipe_to_success_broadcast(cli[ssock[sender_id]], cli[ssock[to_id]], command);
        pipe_occupied[sender_id][to_id] = true;
        pipe[sender_id][to_id] = output;
        return 0;
    }
}

int Database::pipe_from(int from_id, int receiver_id, std::string &input, const std::string &command) {
    if (!pipe_occupied[from_id][receiver_id]) {
        pipe_not_exist_unicast(from_id, receiver_id);
        return 1;
    }
    else {
        pipe_from_success_broadcast(cli[ssock[from_id]], cli[ssock[receiver_id]], command);
        pipe_occupied[from_id][receiver_id] = false;
        input = pipe[from_id][receiver_id];
        return 0;
    }
}


