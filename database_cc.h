#pragma once
#include <arpa/inet.h>
#include <string>
#include <vector>
#include <sys/shm.h>
#include <sys/ipc.h>
#include "client_info_cc.h"
#include "serverlib_cc.h"
#include "message_cc.h"

class Database {
private:
    // Database(int nfds): nfds(nfds), ssock(nfds + 1), cli(nfds + 1), valid(nfds + 1, false),
    //                     pipe(nfds + 1, std::vector<std::string>(nfds + 1)),
    //                     pipe_occupied(nfds + 1, std::vector<bool>(nfds + 1, false)) {}
    Database(): nfds(30) {
        shm_cli = shmget(0, 30*sizeof(Client), IPC_CREAT|0666);
        cli = (Client*) shmat(shm_cli, NULL, 0);
        for (int id = 0; id < 30; ++id)
            cli[id].pid = -1;
        shm_msg = shmget(0, sizeof(Message), IPC_CREAT|0666);
        msg = (Message*) shmat(shm_msg, NULL, 0);
        for (int i = 0; i < 30; ++i) {
            msg->valid_msg[i] = false;
            for (int j = 0; j < 30; ++j)
                msg->pipe_occupied[i][j] = false;
        }
    }

public:
    int nfds;
    // std::vector<bool> valid;
    // std::vector<int> ssock;
    // std::vector<Client> cli;

    // std::vector<std::vector<bool> > pipe_occupied;
    // std::vector<std::vector<std::string> > pipe; // pipe[from][to]

    int shm_cli;
    int shm_msg;
    Client *cli;
    Message *msg;

    ~Database() {
        shmdt(cli);
        shmdt(msg);

        shmctl(shm_cli, IPC_RMID, NULL);
        shmctl(shm_msg, IPC_RMID, NULL);
    }
    Database(Database const&) = delete;
    void operator=(Database const&) = delete;

    static Database& GetInstance() {
        static Database instance;
        return instance;
    }

    // return id
    // pid, addr
    int new_client(int, struct sockaddr_in);
    // id
    void delete_client(int);
    void kick_client(int);

    // Client& get_client(int);

    void set_name(int, const std::string&);

    // int get_id(int);

    // from_id(sender), to_id, output, command line
    // return 0 if success
    int pipe_to(int, int, const std::string&, const std::string&);

    // from_id, to_id(receiver), the string to return, command line
    // return 0 if success
    int pipe_from(int, int, std::string&, const std::string&);
};
