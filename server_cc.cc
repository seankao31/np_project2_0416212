#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <string>
#include <iostream>
#include <errno.h>

#include "database_cc.h"
#include "login_server_cc.h"
#include "serverlib_cc.h"
#include "shell_cc.h"

void reaper(int sig) {
    int status;
    while (waitpid(-1, &status, WNOHANG) > 0)
        /*empty*/ ;
}

void terminate(int sig) {
    Database& database = Database::GetInstance();

    shmdt(database.cli);
    shmdt(database.msg);

    shmctl(database.shm_cli, IPC_RMID, NULL);
    shmctl(database.shm_msg, IPC_RMID, NULL);
    exit(0);
}

int main(int argc, char const* argv[])
{
    int sockfd, newsockfd, childpid;
    unsigned clilen;
    struct sockaddr_in cli_addr, serv_addr;

    int SERV_TCP_PORT = 7000;
    if (argc != 1) {
        SERV_TCP_PORT = std::stoi(argv[1]);
    }

    // Open a TCP socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "server: can't open stream socket" << std::endl;
    }

    // Bind our local address so that client can send to us
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(SERV_TCP_PORT);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << "server: can't bind local address" << std::endl;
    }

    listen(sockfd, 5);

    signal(SIGCHLD, reaper);
    signal(SIGINT, terminate);
    signal(SIGTERM, terminate);

    Database::GetInstance();

    char *home;
    home = getenv("HOME");
    chdir(home);
    chdir("rwg");

    int pipefd[2];

    while(true) {
        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) {
            std::cerr << "server: accept error" << std::endl;
        }
        pipe(pipefd);
        if ((childpid = fork()) < 0) {
            std::cerr << "server: fork error" << std::endl;
        }
        else if (childpid == 0) {
            close(pipefd[1]);
            read(pipefd[0], &childpid, sizeof(childpid));
            close(pipefd[0]);
            close(sockfd);
            int id = Database::GetInstance().new_client(childpid, cli_addr);
            shell(newsockfd, childpid, id);
            exit(0);
        }
        else {
            close(pipefd[0]);
            write(pipefd[1], &childpid, sizeof(childpid));
            close(pipefd[1]);
        }
        close(newsockfd);
    }

    return 0;
}
