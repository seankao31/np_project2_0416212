#pragma once
#include <errno.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include "client_info_cc.h"

#define MAXLINE 10001

int bufread(int sockfd, char *ptr);

int _readline(int sockfd, char *vptr, size_t maxlen);

int myreadline(int sockfd, std::string &str);

ssize_t writen(int sockfd, const char *vptr, size_t n);

std::string get_ip_port(const Client&);

// return 0 if success
// second string default = ""
int unicast(std::string, int, std::string);

// second string default = ""
void broadcast(std::string, std::string);

// first is caster id, second is non exist id
void user_not_exist_unicast(int, int);

// first is pipe from id, second is existed pipe to id
void pipe_exist_unicast(int, int);
void pipe_not_exist_unicast(int, int);

void pipe_to_success_broadcast(const Client&, const Client&, const std::string&);
void pipe_from_success_broadcast(const Client&, const Client&, const std::string&);

int kick(int, int);

int tell(const Client&, std::string, int);

void yell(const Client&, std::string);

void login_broadcast(const Client&);

void exit_broadcast(const Client&);

void name_success_broadcast(const Client&);

void name_fail_unicast(const std::string&, int);

