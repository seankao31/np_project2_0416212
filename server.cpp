#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <string>
#include <iostream>
#include <errno.h>

#include "serverlib.h"
#include "shell.h"
#include "login_server.h"
#include "client_info.h"
#include "database.h"

int main(int argc, char const* argv[])
{
    int msock, childpid;
    unsigned clilen;
    struct sockaddr_in cli_addr, serv_addr;
    fd_set rfds, afds;

    int SERV_TCP_PORT = 7000;
    if (argc != 1) {
        SERV_TCP_PORT = std::stoi(argv[1]);
    }

    // Open a TCP socket
    if ((msock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "server: can't open stream socket" << std::endl;
    }

    // Bind our local address so that client can send to us
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(SERV_TCP_PORT);

    if (bind(msock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << "server: can't bind local address" << std::endl;
    }

    listen(msock, 5);

    int nfds = getdtablesize(); FD_ZERO(&afds);
    FD_SET(msock, &afds);

    Database::GetInstance(nfds);
    LoginServer::GetInstance();

    int shell_number = nfds;
    Shell shell_pool[shell_number];
    for (int id = 0; id < shell_number; ++id) {
        shell_pool[id] = Shell();
    }

    char *home;
    home = getenv("HOME");
    chdir(home);
    chdir("rwg");

    while(true) {
        memcpy(&rfds, &afds, sizeof(rfds));

        if (select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0) {
            std::cerr << "select: " << strerror(errno) << std::endl;
        }

        if (FD_ISSET(msock, &rfds)) {
            std::cout << "# Server" << std::endl;
            int ssock;
            clilen = sizeof(cli_addr);
            ssock = accept(msock, (struct sockaddr *) &cli_addr, &clilen);
            if (ssock < 0) {
                std::cerr << "server: accept error" << std::endl;
            }
            std::cout << "current fd: " << ssock << std::endl;
            FD_SET(ssock, &afds);
            int sid = LoginServer::GetInstance().acquire(ssock, cli_addr);
            if (sid < 0) {
                std::cerr << "server: no available id" << std::endl;
            }

            std::cout << "New Client" << std::endl;
            std::cout << Database::GetInstance().get_client(ssock).name << std::endl;
            std::cout << Database::GetInstance().get_client(ssock).ip << "/";
            std::cout << Database::GetInstance().get_client(ssock).port << std::endl;

            std::cout << "new client id: " << sid << std::endl;
            std::cout << std::endl << std::endl;
            shell_pool[sid].run_once_init(ssock);
            login_broadcast(Database::GetInstance().get_client(ssock));
            shell_pool[sid].prompt();
        }

        for (int fd = 0; fd < nfds; ++fd) {
            if (fd != msock && FD_ISSET(fd, &rfds)) {
                std::cout << "# Server" << std::endl;
                std::cout << "current fd: " << fd << std::endl;
                int sid = Database::GetInstance().get_id(fd);
                if (sid < 0) {
                    std::cerr << "server: no id active for such fd" << std::endl;
                }
                int status;
                std::cout << std::endl << "# Client id: " << sid << std::endl << std::endl;
                if ((status = shell_pool[sid].run_once()) == STATUS_EXIT) {
                    exit_broadcast(Database::GetInstance().get_client(fd));
                    std::cout << "EXIT" << std::endl << std::endl;
                    LoginServer::GetInstance().release(fd);
                    close(fd);
                    FD_CLR(fd, &afds);
                }
                else {
                    ; // do nothing, continue
                }
            }

        }

        // TODO: ZOMBIE





        //if ((childpid = fork()) < 0) {
            //std::cerr << "server: fork error" << std::endl;
        //}
        //else if (childpid == 0) {
            //close(msock);
            //shell(ssock);
            //exit(0);
        //}
        //else {
            //int status;
            //if (waitpid(childpid, &status, 0) == -1) {
                //std::cerr << "server: zombie" << std::endl;
            //}
        //}
        //close(ssock);
    }

    return 0;
}
