#include <errno.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <arpa/inet.h>
#include <iostream>
#include "serverlib_cc.h"
#include "database_cc.h"

#define MAXLINE 10001

#define DEMO

int read_cnt = 0;
char *read_ptr;
char read_buf[MAXLINE];

int bufread(int sockfd, char *ptr) {
    while (read_cnt <= 0) {
        if ((read_cnt = read(sockfd, read_buf, sizeof(read_buf))) < 0) {
            if (errno == EINTR)
                continue;
            return -1;
        }
        else if (read_cnt == 0)
            return 0;
        read_ptr = read_buf;
    }
    read_cnt--;
    *ptr = *read_ptr++;
    return 1;
}

int _readline(int sockfd, char *vptr, size_t maxlen) {
    char *ptr = vptr;
    char c;
    ssize_t rc, n;
    for (n = 1; n < maxlen; n++) {
        if ((rc = bufread(sockfd, &c)) == 1) {
            *ptr++ = c;
            if (c == '\n')
                break;
        }
        else if (rc == 0) {
            break;
        }
        else
            return -1;
    }
    *ptr = '\0';
    return 0;
}

int myreadline(int sockfd, std::string &str) {
    char cstr[MAXLINE];
    if (_readline(sockfd, cstr, sizeof(read_buf)) == 0) {
        str = std::string(cstr);
        return 0;
    }
    return -1;
}

ssize_t writen(int sockfd, const char *vptr, size_t n) {
    size_t left = n;
    ssize_t written;
    const char *ptr = vptr;

    while (left > 0) {
        if ((written = write(sockfd, ptr, left)) <= 0) {
            if (written < 0 && errno == EINTR) {
                written = 0;
            }
            else {
                return -1;
            }
        }

        left -= written;
        ptr += written;
    }
    return n;
}

std::string get_ip_port(const Client& cli) {
#ifdef DEMO
    return "CGILAB/511";
#else
    char ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &cli.addr.sin_addr, ip, INET_ADDRSTRLEN);
    return std::string(ip) + "/" + std::to_string(cli.addr.sin_port);
#endif
}

int unicast(std::string str, int id, std::string str2 = "") {
    Database& database = Database::GetInstance();
    str = "*** " + str + " ***" + str2 + "\n";
    if (database.cli[id].pid != -1) {
        database.msg->valid_msg[id] = true;
        strcpy(database.msg->msg[id], str.c_str());
        kill(database.cli[id].pid, SIGUSR1);
        return 0;
    }
    else {
        return 1;
    }
}

void broadcast(std::string str, std::string str2 = "") {
    for (int id = 0; id < 30; ++id) {
        unicast(str, id, str2);
    }
}

void user_not_exist_unicast(int cast_id, int not_exist_id) {
    std::string str = "Error: user #" + std::to_string(not_exist_id + 1) + " does not exist yet.";
    unicast(str, cast_id);
}

void pipe_exist_unicast(int pipe_from_id, int pipe_to_id) {
    std::string str = "Error: the pipe #" + std::to_string(pipe_from_id + 1)
                      + "->#" + std::to_string(pipe_to_id + 1) + " already exists.";
    unicast(str, pipe_from_id);
}

void pipe_not_exist_unicast(int pipe_from_id, int pipe_to_id) {
    std::string str = "Error: the pipe #" + std::to_string(pipe_from_id + 1)
                      + "->#" + std::to_string(pipe_to_id + 1) + " does not exist yet.";
    unicast(str, pipe_to_id);
}

void pipe_to_success_broadcast(const Client &pipe_from_cli, const Client &pipe_to_cli, const std::string &command) {
    std::string str = std::string(pipe_from_cli.name) + " (#" + std::to_string(pipe_from_cli.id + 1) + ") just piped \'"
                      + command + "\' to " + pipe_to_cli.name + " (#" + std::to_string(pipe_to_cli.id + 1) + ")";
    broadcast(str);
}

void pipe_from_success_broadcast(const Client &pipe_from_cli, const Client &pipe_to_cli, const std::string &command) {
    std::string str = std::string(pipe_to_cli.name) + " (#" + std::to_string(pipe_to_cli.id + 1) + ") just received from "
                      + pipe_from_cli.name + " (#" + std::to_string(pipe_from_cli.id + 1) + ") by \'" + command + "\'";
    broadcast(str);
}

int kick(int kicker_id, int kicked_id) {
    Database& database = Database::GetInstance();
    if (database.cli[kicked_id].pid == -1) {
        std::string str = "user #" + std::to_string(kicked_id + 1) + " doesn't exist yet";
        unicast(str, kicker_id);
        return 0;
    }
    std::string str = "user #" + std::to_string(kicker_id + 1) + " kicked " + "user #" + std::to_string(kicked_id + 1);
    broadcast(str);
    kill(database.cli[kicked_id].pid, SIGUSR2);
    return 0;
}

int tell(const Client &cli, std::string msg, int id) {
    std::string str = std::string() + cli.name + " told you";
    if (unicast(str, id, ": " + msg) != 0) {
        user_not_exist_unicast(cli.id, id);
        return 1;
    }
    return 0;
}

void yell(const Client &cli, std::string msg) {
    std::string str = std::string() + cli.name + " yelled";
    broadcast(str, ": " + msg);
}

void login_broadcast(const Client &cli) {
    std::string str = std::string() + "User \'" + cli.name + "\' entered from " + get_ip_port(cli) + ".";
    broadcast(str);
    std::cout << str << std::endl;
}

void exit_broadcast(const Client &cli) {
    std::string str = std::string() + "User \'" + cli.name + "\' left.";
    broadcast(str);
    std::cout << str << std::endl;
}

void name_success_broadcast(const Client &cli) {
    std::string str = "User from " + get_ip_port(cli) + " is named \'" + cli.name + "\'.";
    broadcast(str);
    std::cout << str << std::endl;
}

void name_fail_unicast(const std::string &name, int id) {
    std::string str = "User \'" + name + "\' already exists.";
    unicast(str, id);
    std::cout << str << std::endl;
}

