OBJ = server.o serverlib.o database.o
OBJ_CC = server_cc.o database_cc.o serverlib_cc.o
DEPS = serverlib.h shell.h login_server.h database.h client_info.h
DEPS_CC = serverlib_cc.h shell_cc.h login_server_cc.h database_cc.h client_info_cc.h
FLAGS = -std=c++11

all: server server_cc

server: $(OBJ)
	g++ -o $@ $+ $(FLAGS)

server_cc: $(OBJ_CC)
	g++ -o $@ $+ $(FLAGS)

%.o: %.cpp $(DEPS)
	g++ -c -o $@ $< $(FLAGS)

%.o: %.cc $(DEPS_CC)
	g++ -c -o $@ $< $(FLAGS)

clean:
	rm -f server server_cc $(OBJ) $(OBJ_CC) my_output*
