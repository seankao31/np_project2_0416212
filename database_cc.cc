#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include "database_cc.h"
#include "message_cc.h"
#include "serverlib_cc.h"

int Database::new_client(int pid, struct sockaddr_in addr) {
    int id;
    for (id = 0; id < 30; ++id) {
        if (cli[id].pid == -1) {
            cli[id].id = id;
            cli[id].pid = pid;
            cli[id].addr = addr;
            strcpy(cli[id].name, "(no name)");
            break;
        }
    }
    // cli[fd].name = "(no name)";
// #ifdef DEMO
//     cli[fd].ip = "CGILAB";
//     cli[fd].port = 511;
// #else
    // char tmp_ip[INET_ADDRSTRLEN];
    // inet_ntop(AF_INET, &addr.sin_addr, tmp_ip, INET_ADDRSTRLEN);
    // cli[fd].ip = tmp_ip;
    // cli[fd].port = addr.sin_port;
// #endif
    // ssock[id] = fd;
    // valid[id] = true;
    return id;
}

void Database::delete_client(int id) {
    exit_broadcast(cli[id]);
    cli[id].pid = -1;
    msg->valid_msg[id] = false;
    for (int i = 0; i < 30; ++i) {
        msg->pipe_occupied[i][id] = false;
        msg->pipe_occupied[id][i] = false;
    }

    // valid[cli[fd].id] = false;
    // for (int id = 1; id <= nfds; ++id) {
    //     pipe_occupied[cli[fd].id][id] = false;
    //     pipe_occupied[id][cli[fd].id] = false;
    // }
}

void Database::kick_client(int id) {
    cli[id].pid = -1;
    msg->valid_msg[id] = false;
    for (int i = 0; i < 30; ++i) {
        msg->pipe_occupied[i][id] = false;
        msg->pipe_occupied[id][i] = false;
    }
}
// Client& Database::get_client(int fd) {
//     if (valid[cli[fd].id])
//         return cli[fd];
//     std::cerr << "Can Not Get Client: Invalid Client" << std::endl;
// }

void Database::set_name(int id_to_set, const std::string &str) {
    for (int id = 0; id < 30; ++id) {
        if (cli[id].pid != -1 && id != id_to_set && str == cli[id].name) {
            name_fail_unicast(str, id_to_set);
            return;
        }
    }
    strcpy(cli[id_to_set].name, str.c_str());
    name_success_broadcast(cli[id_to_set]);
}

// int Database::get_id(int fd) {
//     if (valid[cli[fd].id])
//         return cli[fd].id;
//     return -1;
// }

int Database::pipe_to(int sender_id, int to_id, const std::string &output, const std::string &command) {
    if (cli[to_id].pid == -1) {
        user_not_exist_unicast(sender_id, to_id);
        return 1;
    }
    else if (msg->pipe_occupied[sender_id][to_id]) {
        pipe_exist_unicast(sender_id, to_id);
        return 1;
    }
    else {
        pipe_to_success_broadcast(cli[sender_id], cli[to_id], command);
        msg->pipe_occupied[sender_id][to_id] = true;
        strcpy(msg->pipe[sender_id][to_id], output.c_str());
        return 0;
    }
}

int Database::pipe_from(int from_id, int receiver_id, std::string &input, const std::string &command) {
    if (!msg->pipe_occupied[from_id][receiver_id]) {
        pipe_not_exist_unicast(from_id, receiver_id);
        return 1;
    }
    else {
        pipe_from_success_broadcast(cli[from_id], cli[receiver_id], command);
        msg->pipe_occupied[from_id][receiver_id] = false;
        input = msg->pipe[from_id][receiver_id];
        return 0;
    }
}


