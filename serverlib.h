#pragma once
#include <string>
#include "client_info.h"
#include "database.h"

#define MAXLINE 10001

int bufread(int, char*);

int _readline(int, char*, size_t);

int myreadline(int, std::string&);

ssize_t writen(int, const char*, size_t);

// return 0 if success
// second string default = ""
int unicast(std::string, int, std::string);

// second string default = ""
void broadcast(std::string, std::string);

// first is caster id, second is non exist id
void user_not_exist_unicast(int, int);

// first is pipe from id, second is existed pipe to id
void pipe_exist_unicast(int, int);
void pipe_not_exist_unicast(int, int);

void pipe_to_success_broadcast(const Client&, const Client&, const std::string&);
void pipe_from_success_broadcast(const Client&, const Client&, const std::string&);

int tell(const Client&, std::string, int);

void yell(const Client&, std::string);

void login_broadcast(const Client&);

void exit_broadcast(const Client&);

void name_success_broadcast(const Client&);

void name_fail_unicast(const std::string&, int);

