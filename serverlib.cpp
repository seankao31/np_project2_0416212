#include <errno.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include "serverlib.h"
#include "database.h"

int read_cnt = 0;
char *read_ptr;
char read_buf[MAXLINE];

int bufread(int sockfd, char *ptr) {
    while (read_cnt <= 0) {
        if ((read_cnt = read(sockfd, read_buf, sizeof(read_buf))) < 0) {
            if (errno == EINTR)
                continue;
            return -1;
        }
        else if (read_cnt == 0)
            return 0;
        read_ptr = read_buf;
    }
    read_cnt--;
    *ptr = *read_ptr++;
    return 1;
}

int _readline(int sockfd, char *vptr, size_t maxlen) {
    char *ptr = vptr;
    char c;
    ssize_t rc, n;
    for (n = 1; n < maxlen; n++) {
        if ((rc = bufread(sockfd, &c)) == 1) {
            *ptr++ = c;
            if (c == '\n')
                break;
        }
        else if (rc == 0) {
            break;
        }
        else
            return -1;
    }
    *ptr = '\0';
    return 0;
}

int myreadline(int sockfd, std::string &str) {
    char cstr[MAXLINE];
    if (_readline(sockfd, cstr, sizeof(read_buf)) == 0) {
        str = std::string(cstr);
        return 0;
    }
    return -1;
}

ssize_t writen(int sockfd, const char *vptr, size_t n) {
    size_t left = n;
    ssize_t written;
    const char *ptr = vptr;

    while (left > 0) {
        if ((written = write(sockfd, ptr, left)) <= 0) {
            if (written < 0 && errno == EINTR) {
                written = 0;
            }
            else {
                return -1;
            }
        }

        left -= written;
        ptr += written;
    }
    return n;
}

int unicast(std::string str, int id, std::string str2 = "") {
    str = "*** " + str + " ***" + str2 + "\n";
    if (Database::GetInstance().valid[id]) {
        writen(Database::GetInstance().ssock[id], str.c_str(), str.length());
        return 0;
    }
    else {
        return 1;
    }
}

void broadcast(std::string str, std::string str2 = "") {
    for (int id = 1; id <= Database::GetInstance().nfds; ++id) {
        if (Database::GetInstance().valid[id]) {
            unicast(str, id, str2);
        }
    }
}

void user_not_exist_unicast(int cast_id, int not_exist_id) {
    std::string str = "Error: user #" + std::to_string(not_exist_id) + " does not exist yet.";
    unicast(str, cast_id);
}

void pipe_exist_unicast(int pipe_from_id, int pipe_to_id) {
    std::string str = "Error: the pipe #" + std::to_string(pipe_from_id)
                      + "->#" + std::to_string(pipe_to_id) + " already exists.";
    unicast(str, pipe_from_id);
}

void pipe_not_exist_unicast(int pipe_from_id, int pipe_to_id) {
    std::string str = "Error: the pipe #" + std::to_string(pipe_from_id)
                      + "->#" + std::to_string(pipe_to_id) + " does not exist yet.";
    unicast(str, pipe_to_id);
}

void pipe_to_success_broadcast(const Client &pipe_from_cli, const Client &pipe_to_cli, const std::string &command) {
    std::string str = pipe_from_cli.name + " (#" + std::to_string(pipe_from_cli.id) + ") just piped \'"
                      + command + "\' to " + pipe_to_cli.name + " (#" + std::to_string(pipe_to_cli.id) + ")";
    broadcast(str);
}

void pipe_from_success_broadcast(const Client &pipe_from_cli, const Client &pipe_to_cli, const std::string &command) {
    std::string str = pipe_to_cli.name + " (#" + std::to_string(pipe_to_cli.id) + ") just received from "
                      + pipe_from_cli.name + " (#" + std::to_string(pipe_from_cli.id) + ") by \'" + command + "\'";
    broadcast(str);
}

int tell(const Client &cli, std::string msg, int id) {
    std::string str = cli.name + " told you";
    if (unicast(str, id, ": " + msg) != 0) {
        user_not_exist_unicast(cli.id, id);
        return 1;
    }
    return 0;
}

void yell(const Client &cli, std::string msg) {
    std::string str = cli.name + " yelled";
    broadcast(str, ": " + msg);
}

void login_broadcast(const Client &cli) {
    std::string str = "User \'" + cli.name + "\' entered from " + cli.ip + "/" + std::to_string(cli.port) + ".";
    broadcast(str);
}

void exit_broadcast(const Client &cli) {
    std::string str = "User \'" + cli.name + "\' left.";
    broadcast(str);
}

void name_success_broadcast(const Client &cli) {
    std::string str = "User from " + cli.ip + "/" + std::to_string(cli.port) + " is named \'" + cli.name + "\'.";
    broadcast(str);
}

void name_fail_unicast(const std::string &name, int id) {
    std::string str = "User \'" + name + "\' already exists.";
    unicast(str, id);
}

