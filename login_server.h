#pragma once
#include <arpa/inet.h>
#include <string>
#include <vector>
#include "database.h"
#include "client_info.h"

class LoginServer {
private:
    LoginServer() {}

public:
    ~LoginServer() {}
    LoginServer(LoginServer const&) = delete;
    void operator=(LoginServer const&) = delete;

    static LoginServer& GetInstance() {
        static LoginServer instance;
        return instance;
    }

    int acquire(int fd, struct sockaddr_in addr) {
        for (int id = 1; id <= Database::GetInstance().nfds; ++id) {
            if (!Database::GetInstance().valid[id]) {
                Database::GetInstance().new_client(fd, id, addr);
                return id;
            }
        }
        return -1;
    }

    void release(int fd) {
        Database::GetInstance().delete_client(fd);
    }
};
