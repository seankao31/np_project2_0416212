#pragma once
#include <arpa/inet.h>
#include <string>

class Client {
public:
    int id;
    std::string name;
    std::string ip;
    int port;
};

